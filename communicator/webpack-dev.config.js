var ExtractTextPlugin = require("extract-text-webpack-plugin");
var _ = require('underscore');
const path = require('path');


console.log('using dev config');

module.exports = _.extendOwn(require("./webpack.config"),{
    externals: null,
    module: {
        loaders: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                loader: 'babel-loader?presets[]=es2015,presets[]=react!ts-loader'
            },
            { test: /\.scss/,
                loaders: [
                    'style-loader',
                    'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]--[hash:base64:5]',
                    'postcss-loader',
                    'sass-loader'
                ],
            },
            {
                test: /\.json$/,
                loader: 'json'
            },
            {
                test: /\.svg$/,
                loader: "url-loader?limit=10000&mimetype=image/svg+xml"
            }
        ]
    },
    postcss: [],
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.jsx', '.scss', '.json'],
        modulesDirectories: [
            'node_modules',
            path.resolve(__dirname, './node_modules')
        ],
        alias: {
            config:path.join(__dirname, 'config', process.env.ENV || 'local'),
            libwmc: path.resolve(__dirname, './dist/scripts/bundle')
        }
    }
});
