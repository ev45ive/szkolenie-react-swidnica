
var gulp = require('gulp');
var webpack = require('webpack');
var webpackDevServer = require('webpack-dev-server');
var webpackConfig = require('../webpack.config')

gulp.task('serve', function(){

	// webpack dev config

	// webpack compiler
	var compiler = webpack(webpackConfig);

	// webpack dev server
	// https://webpack.github.io/docs/webpack-dev-server.html
	var devServer = new webpackDevServer(compiler,{
		publicPath: '/',
		stats:{
			colors: true
		},

		//https://webpack.github.io/docs/webpack-dev-server.html#proxy
		proxy: {
		  '/webpack/*': {
		    target: 'http://localhost:9000',
		    secure: false
		  }
		}
	})

	// webpack server with livereload
	devServer.listen(8080,'localhost', function(err){
		if(err)
		console.error(err)
	})


});

gulp.task('webpack', function(callback){

	// webpack dev config

	// webpack compiler
	var compiler = webpack(webpackConfig);

	compiler.run(function(){
		callback();
	});
});