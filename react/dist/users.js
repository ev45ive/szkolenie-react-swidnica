[
	{
		"id": 0,
		"name": "Zenobiusz",
		"email": "zenek@rst.com.pl",
		"team": "react",
		"created": "12-05-2015 15:43:00",
		"bio": "Zenon is our local React Guru"
	},
	{
		"id": 1,
		"name": "Arkadiusz",
		"email": "arek@rst.com.pl",
		"team": "angular",
		"created": "04-03-2016 12:43:00",
		"bio": "Arek is not amused with your React wizardry"
	},
	{
		"id": 2,
		"name": "Pani Zofia",
		"email": "zofia@rst.com.pl",
		"team": "dev-ops",
		"created": "06-03-2016 11:12:00",
		"bio": "She watches your every move"
	}
]