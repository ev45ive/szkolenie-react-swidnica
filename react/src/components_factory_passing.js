import React from 'react';
import ReactDOM from 'react-dom';


var lista = [
	'Mleko',
	'Jajka',
	'Chleb',
];

let ListItem = (props) => <li>{props.children}</li>;

let ListItem2 = (props) => <li> &times; {props.children}</li>;


class ListItem3 extends React.Component {
	render(){
		return <li>!{this.props.children}</li>;
	}
}

class Content extends React.Component {
	static propTypes = {

		// Component factory
		item: React.PropTypes.func.isRequired,

		// Component Instance
		//children: React.PropTypes.element.isRequired
	}

	constructor(props){
		super();
		this.item = props.item;
	}

	item = ListItem;

	render() {
		var Item = this.item;

		return <section>
		 <h1>Lista zakupów</h1>
			<h2 id="food" className="subcaption">Spożywcze</h2>
			<ul>
			 { this.props.data.map((item)=> <Item key={item}>{item}</Item> ) }
			</ul>
		</section>
	}
};

//React.createElement('ListItem3') == <ListItem3/>;

var dom = ReactDOM.render( <Content data={lista} item={ListItem2}></Content> ,document.getElementById('app'));
