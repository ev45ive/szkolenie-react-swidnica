import React from 'react';
import ReactDOM from 'react-dom';
import { Grid, Row, Col, PageHeader } from 'react-bootstrap';

//import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap/dist/css/bootstrap-theme.css';

import users from './users';
import UsersList from './userslist'


import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
class UserForm extends React.Component{

	componentWillMount(){
		this.setState({
			//user: this.props.user
			/*user: { 
				name:'',
				email: "",
				birthday: '',
				team: "",
				created: "",
				bio: ""
			}*/
		})
	}

	componentWillReceiveProps(nextProps){
		console.log(nextProps);
	}

	getValidationState(){
	    const length = this.state.user.name.length;
	    if (length > 10) return 'success';
	    else if (length > 5) return 'warning';
	    else if (length > 0) return 'error';
	}

	handleChange = (e)=>{
		debugger;
		this.setState({user:{...user,name: e.target.value }});
	}

	render(){
		return <form>
		<FormGroup
		  controlId="formBasicText"
		  validationState={this.getValidationState()}>
		  <ControlLabel>Working example with validation</ControlLabel>
		  <FormControl
		    type="text"
		    value={this.state.user.name}
		    placeholder="Enter text"
		    onChange={this.handleChange}/>
		  <FormControl.Feedback />
		  <FormControl
		    type="text"
		    value={this.state.user.email}
		    placeholder="Enter text"
		    onChange={this.handleChange}/>
		  <FormControl.Feedback />
		  <FormControl
		    type="text"
		    value={this.state.user.birthday}
		    placeholder="Enter text"
		    onChange={this.handleChange}/>
		  <FormControl.Feedback />
	      <FormControl componentClass="select" placeholder="select">
	        <option value="select">select</option>
	        <option value="other">...</option>
	      </FormControl>
	      <FormControl componentClass="textarea" placeholder="select" value={this.state.user.bio}>
	      </FormControl>
		</FormGroup>
		</form>
		{this.state.user}
	}

}

class App extends React.Component{
	constructor(){
		super();
	}

	componentWillMount(){
		this.setState({user: { name:'No User Selected'}})
	}

	selectUser = (user, event) => {
		this.setState({user})
	}

	render(){
		return (<Grid>
			<Row>
				<Col xs={12} md={6}>
					<UsersList handleSelect={this.selectUser} header="Users List" data={this.props.users}/>
				</Col>
				<Col xs={12} md={6}>
					<PageHeader> Selected User</PageHeader>
					{ this.state.user.name }
					<UserForm user={this.state.user} />
				</Col>
			</Row>
		</Grid>);
	}
}

ReactDOM.render( <App users={users} /> ,document.getElementById('app'));