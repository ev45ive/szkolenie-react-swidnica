import React from 'react';
import { PageHeader, ListGroup, ListGroupItem } from 'react-bootstrap';

var UserItem = (props) => <div><div>{props.user.name}</div></div>;

export default class UsersList extends React.Component{
	constructor(){
		super();
	}

	handleSelect = (user, event) => {
		this.props.handleSelect(user, event);
	}

	render(){
		return <div>
			 <PageHeader>{this.props.header || 'A List'}</PageHeader>
			 <ListGroup>
			    {this.props.data.map((user) => {
			    	return <ListGroupItem onClick={(e)=>this.handleSelect(user,e)}>
			    		<UserItem key={user.id} user={user} />
		    		</ListGroupItem>
	    		})}
	 		 </ListGroup>
    	</div>;
	}
}