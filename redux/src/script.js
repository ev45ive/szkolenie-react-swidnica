import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, combineReducers, applyMiddleware } from 'redux';

let id = (id) => id;

let items = (items = [], action) => {
  // Flux capacitor (work in progress ;-) )
  return (({
	  'ADD_ITEM': ()=>  [...items, 1],
	  'REMOVE_ITEM': ()=> items.slice(0,-1)
  })[action.type] || id)(items);
}

let counter = (counter = 0, action) => {
  switch (action.type) {
  case 'INCREMENT':
    return counter + 1
  case 'DECREMENT':
    return counter - 1
  default:
  	return counter
  }
}
import axios from 'axios';
import thunk from 'redux-thunk';

let usersReducer = (users = [], action) => (({
	"USERS_LOAD":({users})=> {
		return users;
	},
	"USERS_LOAD_COMPLETED":({users})=> {
		return users;
	},
	"USERS_ADD": ({user}) => [...users, user],
	"USERS_UPDATE":({user})=> {
		let index = users.findIndex(({id})=>id===user.id);
		// items before, item, items after...
		return [...users.slice(0,index),user, ...users.slice(index+1,users.length)]
	}
})[action.type] || id)(action);


// build main app reducer
let appReducer = combineReducers({
	users: usersReducer,
	items,
	counter,
	something: combineReducers({
		counter
	})
});
// derive store from reducer
var store = createStore( appReducer , { 
	users: [],
	//selected: null,
	counter: 0, 
	items: [], 
	something:{ 
		counter:0 
	}
}, applyMiddleware(thunk));


let LoadUsersAction = (url) => (dispatch) => { 

	axios.get('users.json')
	.then((response)=>{
		dispatch({
			type: "USERS_LOAD_COMPLETED", 
			users: response.data 
		});
	});
}

store.dispatch(LoadUsersAction('users.json'))


/*
var user = {...users[0]};
user.name = "Nowe"
store.dispatch({ type: 'USERS_UPDATE', user })*/

/*
store.dispatch({ type: 'INCREMENT' })
store.dispatch({ type: 'INCREMENT' })
store.dispatch({ type: 'DECREMENT' })
store.dispatch({ type: 'ADD_ITEM' })
store.dispatch({ type: 'ADD_ITEM' })
store.dispatch({ type: 'REMOVE_ITEM' })
*/


import UsersList from './userslist'
import UserForm from './userform'

const messageOnEmpty = (isEmpty, message, attrs) => (props)=> {
	return isEmpty? props.children : <div {...attrs}>{message}</div>;
}

const Container = (props) => {
	var Component = props.component;
	return  <Component {...props}>{props.children}</Component>;
}

class App extends React.Component{
	constructor(){
		super();
	}

	state = {
		users: []
	}

	componentWillMount(){
		this.store = this.props.store;


		this.store.subscribe(() => {
			var state = this.store.getState();

			this.setState({
				users: state.users
			})
		});

	}	

	selectUser = (user, event) => {
		this.setState({user})
	}

	newUser = (event) => {
		this.setState({user:{
			id: 'c'+ parseInt(Math.random()*1000*1000),
			name: '',
			email:'',
			team:'',
			bio:''
		}})
	}

	updateSelectedUser = (user) => {

		var users  = this.state.users;
		var index = users.findIndex((u)=>u.id === user.id)

		if(index === -1){
			this.setState({users: [...users, user], user })
		}else{
			// replace element in list
			users.splice(index,1,user)
			// update state with list
			this.setState({users, user})
		}
		console.log(this.state.users)
		
	}

	render(){
		return (
		<div className="container">
			<div className="row">
				<div className="col-xs-12 col-md-6">
					<UsersList handleSelect={this.selectUser} header="Users List" data={this.state.users}/>
					<a className="btn btn-primary pull-right" onClick={this.newUser}>Create New</a>
				</div>
				<div className="col-xs-12 col-md-6">
					<h1> Selected User</h1>
						<p>Parent Name - {this.state.user && this.state.user.name}</p>
						<Container component={messageOnEmpty(this.state.user,'Please Select User',{className:'text-danger'})}>
							<UserForm user={this.state.user} handleSave={this.updateSelectedUser} />
						</Container>
				</div>
			</div>
		</div>
		);
	}
}

ReactDOM.render( <App store={store} /> ,document.getElementById('app'));