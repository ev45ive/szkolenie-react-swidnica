import React from 'react';
import ReactDOM from 'react-dom';


export default class UserForm extends React.Component{

	componentWillMount(){
		this.handleSave = this.props.handleSave; 

		this.setState({
			user: this.props.user || {
				name:'',
				email: '',
			},
			original: this.props.user
		});
	}

	componentWillReceiveProps(nextProps){
		this.setState({
			user: nextProps.user,
			original: nextProps.user
		})
	}

	updateField = (field) => (e) => {
		let value = e.target.value;
		this.setState((s) => {
			return {user: {...s.user, [field]: value}};
		});
	}

	revert = (e) => {
		this.setState({
			user: this.state.original
		})
	}

	save = (e) => {
		this.handleSave({...this.state.user}); 
	}

	render(){
		return <form>
			<p>Form name - {this.state.user.name }</p>
			{this.state.user.id}
			<Field id="name" 
				label="Name" 
				placeholder="Enter your name"
				value={this.state.user.name} 
				onChange={this.updateField('name')}></Field>
			<Field id="email" 
				label="Email" 
				placeholder="Enter your email"
				value={this.state.user.email} 
				onChange={this.updateField('email')}
				Component={Input}></Field>
			<Field id="team" 
				label="Team" 
				placeholder="Select your team"
				value={this.state.user.team} 
				onChange={this.updateField('team')} 
				Component={Select}>
					<option value="react">React</option>
					<option value="angular">Angular</option>
			</Field>
			<Field id="bio" 
				label="Bio" 
				placeholder="Enter your bio"
				value={this.state.user.bio} 
				onChange={this.updateField('bio')}
				Component={Textarea}></Field>
			<div className="btn-group pull-right">
				<a className="btn btn-danger" onClick={this.revert}>Reset</a>
				<a className="btn btn-success" onClick={this.save}>Save</a>
			</div>
		</form>
		{this.state.user}
	}
}

class DOMCaptcha extends React.Component{

	componentDidMount(){
		var el = ReactDOM.findDOMNode(this.refs.myInput)
		el.addEventListener('input',(e)=>console.log(e))
	}

	componentDidUpdate(prevProps = {}, prevState = {}){
		console.log('update', ReactDOM.findDOMNode(this.refs.myInput) )
	}

	render(){
		return <div><input ref="myInput" /></div>;
	}
}

const Input = (p) => <input type="text" {...p}/>;
const Textarea = (p) => <textarea {...p}></textarea>;
const Select = (p) => <select {...p}>{p["data-options"]}</select>;

const Field = (props) => {
	let {
		id,
		name,
		type = 'text',
		label,
		placeholder,
		value,
		className, 
		onChange,
		extra,
		Component = Input
	} = props;

	let attrs = { id, type, name, value, placeholder, onChange, "data-options": props.children, ...extra };

	return <div className="form-group">
	    <label htmlFor={id}>{label}</label>
	    <Component className="form-control" {...attrs}/>
	  </div>
}
