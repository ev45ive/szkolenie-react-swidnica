import React from 'react';
import ReactDOM from 'react-dom';


import Reflux from 'reflux';
import axios from 'axios';

var UsersActions = Reflux.createActions({
	"usersLoad":{
	    asyncResult: true,
	},
	"usersLoaded":{},
    "userUpdated":{},
    "userEdited":{},
    "userAdded":{}
  });

var usersStore = Reflux.createStore({

    // Initial setup
    init: function() {
    	this.state = {
    		users: []
    	}

        this.listenToMany(UsersActions)
    },

    onUsersLoad: function(){
    	axios.get('users.json')
    	.then((r)=>r.data)
    	.then(UsersActions.usersLoad.completed, UsersActions.usersLoad.failed)
    },

    onUsersLoadCompleted: function(data){
    	UsersActions.usersLoaded(data);
    },

    onUsersLoadFailed: function(err){

    },

    onUsersLoaded: function(users){
    	this.state.users = users;
    	this.trigger(this.state);
    },

    onUserUpdated: function(user){
		var users  = this.state.users;
		var index = users.findIndex((u)=>u.id === user.id)

		if(index === -1){
			UsersActions.userAdded(user);
		}else{
			UsersActions.userEdited(user);
		}
    },

    onUserEdited: function(user){
    	let users = this.state.users;
		var index = users.findIndex( (u)=> u.id === user.id )

		// replace element in list
		users.splice(index,1,user);

    	this.trigger(this.state);
    },

    onUserAdded: function(user) {
    	this.state.users.push(user)
    	this.trigger(this.state);
    }
});

// Registers a console logging callback to the statusStore updates
usersStore.listen(function(status) {
    console.log('status: ', status);
});




var UIActions = Reflux.createActions({
    "userSelected":{},	
})

var uiStore = Reflux.createStore({
	init: function(){
		this.state = {
			users: [],
			selected: null
		};

		// README: https://github.com/reflux/refluxjs#joining-parallel-listeners-with-composed-listenables
		this.joinTrailing(UsersActions.userEdited, (actions) => this.onUserSelected(actions[0]) )

		this.listenTo(usersStore, (other) => {
			this.state.users = other.users;
	    	this.trigger(this.state);
		});

        this.listenToMany(UIActions)
	},

    onUserSelected: function(user){
    	this.state.selected = user;
    	this.trigger(this.state);
    },
});

import UsersList from './userslist'
import UserForm from './userform'

const messageOnEmpty = (isEmpty, message, attrs) => (props)=> {
	return isEmpty? props.children : <div {...attrs}>{message}</div>;
}

const Container = (props) => {
	var Component = props.component;
	return  <Component {...props}>{props.children}</Component>;
}

class App extends React.Component{
	constructor(){
		super();
	}

	componentWillMount(){
		this.store = this.props.store;

		UsersActions.usersLoad();

		// listen UI changes
		this.store.listen((storeState) => {
			this.setState({
				user: storeState.selected,
				users: storeState.users
			})
		});

		this.setState({
			users: [],
			user: null
		})
	}

	selectUser = (user, event) => {
		UIActions.userSelected(user);
	}

	newUser = (event) => {
		this.setState({user:{
			id: 'c'+ parseInt(Math.random()*1000*1000),
			name: '',
			email:'',
			team:'',
			bio:''
		}})
	}

	updateSelectedUser = (user) => {
		UsersActions.userUpdated(user)		
	}

	render(){
		return (
		<div className="container">
			<div className="row">
				<div className="col-xs-12 col-md-6">
					<UsersList handleSelect={this.selectUser} header="Users List" user={this.state.user} data={this.state.users}/>
					<a className="btn btn-primary pull-right" onClick={this.newUser}>Create New</a>
				</div>
				<div className="col-xs-12 col-md-6">
					<h1> Selected User</h1>
						<p>Parent Name - {this.state.user && this.state.user.name}</p>
						<Container component={messageOnEmpty(this.state.user,'Please Select User',{className:'text-danger'})}>
							<UserForm user={this.state.user} handleSave={this.updateSelectedUser} />
						</Container>
				</div>
			</div>
		</div>
		);
	}
}

ReactDOM.render( <App store={uiStore} /> ,document.getElementById('app'));