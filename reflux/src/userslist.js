import React from 'react';
import classnames from 'classnames';

var UserItem = (props) => <div><div>{props.user.name}</div></div>;

export default class UsersList extends React.Component{
	constructor(){
		super();
	}

	componentWillMount(){
		this.setState({})
	}

	handleSelect = (user) => (event) => {
		this.props.handleSelect(user, event);
	}

	render(){
		let selected = this.props.user;
		return <div>
			 <h1>{this.props.header || 'A List'}</h1>
			 <div className="list-group">
			    {this.props.data.map((user) => {
			    	return <a className={classnames("list-group-item",{'active': user == selected })} key={user.id} onClick={this.handleSelect(user)}>
			    		<UserItem user={user} />
		    		</a>
	    		})}
	 		 </div>
    	</div>;
	}
}