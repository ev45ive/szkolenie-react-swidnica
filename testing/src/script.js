import React from 'react';
import ReactDOM from 'react-dom';

//import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap/dist/css/bootstrap-theme.css';

import users from './users';
import UsersList from './userslist'
import UserForm from './userform'

const messageOnEmpty = (isEmpty, message, attrs) => (props)=> {
	return isEmpty? props.children : <div {...attrs}>{message}</div>;
}

const Container = (props) => {
	var Component = props.component;
	return  <Component {...props}>{props.children}</Component>;
}

class App extends React.Component{
	constructor(){
		super();
	}

	componentWillMount(){
		this.setState({
			users: this.props.users
		})
	}

	selectUser = (user, event) => {
		this.setState({user})
	}

	newUser = (event) => {
		this.setState({user:{
			id: 'c'+ parseInt(Math.random()*1000*1000),
			name: '',
			email:'',
			team:'',
			bio:''
		}})
	}

	updateSelectedUser = (user) => {

		var users  = this.state.users;
		var index = users.findIndex((u)=>u.id === user.id)

		if(index === -1){
			this.setState({users: [...users, user], user })
		}else{
			// replace element in list
			users.splice(index,1,user)
			// update state with list
			this.setState({users, user})
		}
		console.log(this.state.users)
		
	}

	render(){
		return (
		<div className="container">
			<div className="row">
				<div className="col-xs-12 col-md-6">
					<UsersList handleSelect={this.selectUser} header="Users List" data={this.state.users}/>
					<a className="btn btn-primary pull-right" onClick={this.newUser}>Create New</a>
				</div>
				<div className="col-xs-12 col-md-6">
					<h1> Selected User</h1>
						<p>Parent Name - {this.state.user && this.state.user.name}</p>
						<Container component={messageOnEmpty(this.state.user,'Please Select User',{className:'text-danger'})}>
							<UserForm user={this.state.user} handleSave={this.updateSelectedUser} />
						</Container>
				</div>
			</div>
		</div>
		);
	}
}

ReactDOM.render( <App users={users} /> ,document.getElementById('app'));