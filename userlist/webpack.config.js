var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var webpack = require('webpack');
var precss       = require('precss');
var autoprefixer = require('autoprefixer');

module.exports = {
    context: path.join(__dirname,'src'),
    entry: "./script.js",
    output: {
        path: path.join( __dirname, 'dist/'),
        filename: "dist.js"
    },
    module: {
        loaders: [
            { test: /\.js|\.jsx/, loader: "babel", exclude: /node_modules/  },
            { test: /\.css$/, 
              loader: ExtractTextPlugin.extract('style', ['css', 'postcss'/*,'sass'*/]  ),
              exclude: /node_modules/
            },
            { test: /\.css$/, 
              loader: ExtractTextPlugin.extract('style', ['css'] ),
            },
            {test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff" },
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" }
        ]   
    },
    postcss: function () {
        return [precss, autoprefixer];
    },
    // Use the plugin to specify the resulting filename (and add needed behavior to the compiler)
    plugins: [
        new ExtractTextPlugin("styles.css"),
        /*new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),*/
        new HtmlWebpackPlugin({
            template: 'index.html',
            minify: {
                html5: true
            }
        })
    ],
    devtool: 'source-map'
};